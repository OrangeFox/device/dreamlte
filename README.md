# OrangeFox Recovery device tree for Samsung S8 aka dreamlte.

## How to build

1. Set up the build environment following instructions from [here](https://wiki.orangefox.tech/en/dev/building)
2. In the root folder of cloned repo you need to clone the device tree:
```bash
git clone https://gitlab.com/ksant0s/android_device_samsung_dreamlte.git -b fox9.0 device/samsung/dreamlte
```
3. To build:
```bash
. build/envsetup.sh && export ALLOW_MISSING_DEPENDENCIES=true && export LC_ALL="C" && lunch omni_dreamlte-eng && mka recoveryimage -j128
```
